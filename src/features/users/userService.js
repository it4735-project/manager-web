import httpService from 'src/services/httpService';

class UserService {
  async getAllUsers() {
    const result = await httpService.request({
      url: '/api/admin/users',
      method: 'GET',
    });

    return result.data;
  }

  async getUserById(id) {
    const result = await httpService.request({
      url: `/api/admin/users/${id}`,
      method: 'GET',
    });

    return result.data;
  }

  async updateUser(data) {
    const result = await httpService.request({
      url: `/api/admin/users/${data.id}`,
      method: 'PUT',
      data,
    });

    return result.data;
  }

  async deleteUser(id) {
    const result = await httpService.request({
      url: `/api/admin/users/${id}`,
      method: 'DELETE',
    });

    return result.data;
  }
}

export default new UserService();
