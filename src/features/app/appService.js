import NiceModal from '@ebay/nice-modal-react';
import LoadingModal from 'src/commons/components/LoadingModal';

class AppService {
  showLoadingModal(
    { fullScreen } = {
      fullScreen: false,
    }
  ) {
    NiceModal.show(LoadingModal, {
      fullScreen,
    });
  }

  hideLoadingModal() {
    NiceModal.hide(LoadingModal);
  }

  getMockAvatar(key) {
    return `https://i.pravatar.cc/150?u=${key}`;
  }
}

export default new AppService();
