import React from 'react';
import { useLocation, Link, Outlet } from 'react-router-dom';
import { MdOutlinePeopleAlt as PeopleIcon } from 'react-icons/md';
import { TbBabyCarriage as CradleIcon } from 'react-icons/tb';

import Navbar from 'src/commons/components/Navbar';
import useAuth from 'src/hooks/useAuth';

function MainLayout() {
  const { pathname } = useLocation();
  useAuth();

  const routes = [
    {
      path: '/users',
      title: 'Users',
      icon: <PeopleIcon className="text-xl" />,
    },
    {
      path: '/products',
      title: 'Products',
      icon: <CradleIcon className="text-xl" />,
    },
  ];

  return (
    <div className="drawer drawer-mobile">
      <input id="mainDrawer" type="checkbox" className="drawer-toggle" />
      <div className="drawer-content flex flex-col items-center">
        <Navbar />
        <Outlet />
      </div>

      <div className="drawer-side">
        <label htmlFor="mainDrawer" className="drawer-overlay"></label>
        <ul className="menu p-4 w-72 bg-base-200 text-base-content">
          <Link
            to="/"
            className="btn btn-ghost normal-case text-2xl flex space-x-2"
          >
            <div className="avatar">
              <div className="w-10">
                <img src="/logo.png" />
              </div>
            </div>
            <div>Smart Cradle</div>
          </Link>

          <div className="divider"></div>

          {routes.map((route, index) => (
            <li key={index} className="mb-1">
              <Link
                to={route.path}
                className={
                  pathname === route.path ? 'bg-primary text-white' : ''
                }
              >
                {route.icon}
                {route.title}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default MainLayout;
