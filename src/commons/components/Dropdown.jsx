import React from 'react';
import { twMerge } from 'tailwind-merge';

export function DropdownItem({ children, ...rest }) {
  return <li {...rest}>{children}</li>;
}

export function DropdownContent({ className, children, ...rest }) {
  return (
    <ul
      tabIndex={0}
      className={twMerge(
        'dropdown-content menu rounded-box menu-compact mt-3 bg-base-100 p-2 shadow-xl',
        className
      )}
      {...rest}
    >
      {children}
    </ul>
  );
}

export function Dropdown({ className, children, labelComponent, ...rest }) {
  return (
    <div className={twMerge('dropdown', className)} {...rest}>
      {labelComponent}
      {children}
    </div>
  );
}
