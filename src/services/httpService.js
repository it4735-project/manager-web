import { API_ENDPOINT, ACCESS_TOKEN_KEY } from 'src/app/constants';
import axios from 'axios';
import Cookies from 'js-cookie';

class HttpService {
  constructor() {
    /**
     * @type {import('axios').AxiosInstance}
     * @private
     */
    this.http = axios.create({
      baseURL: API_ENDPOINT,
      timeout: 30000,
    });

    this.http.interceptors.request.use(
      (config) => {
        const headers = config.headers;
        const accessToken = Cookies.get(ACCESS_TOKEN_KEY);

        if (accessToken) {
          headers['Authorization'] = `Bearer ${accessToken}`;
        }

        return { ...config, headers: config.headers };
      },
      (error) => {
        return Promise.reject(error);
      }
    );
  }

  async request({ url, params, data, method }) {
    const config = {
      url,
      method,
      params,
      data,
    };

    const response = await this.http.request(config);

    return response.data;
  }
}

export default new HttpService();
