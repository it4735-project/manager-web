import React from 'react';

export default function SignUpPage() {
  return (
    <>
      <div className="flex min-h-full items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="w-full max-w-md space-y-8">
          <div>
            {/* <img
              className="mx-auto h-12 w-auto"
              src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
              alt="Your Company"
            /> */}
            <h1 className="text-center text-5xl font-bold text-primary tracking-tight">
              Smart Cradle
            </h1>
            <h2 className="mt-6 text-center text-2xl tracking-tight">
              Sign in to your account
            </h2>
          </div>
          <form className="mt-8 space-y-3" action="#" method="POST">
            <input type="hidden" name="remember" defaultValue="true" />
            <div className="form-control w-full">
              <input
                type="text"
                placeholder="Username"
                required
                className="input input-bordered w-full"
              />
            </div>

            <div className="form-control w-full">
              <input
                type="password"
                placeholder="Password"
                required
                className="input input-bordered w-full"
              />
            </div>

            <div className="flex items-center justify-between">
              <div className="form-control">
                <label className="label cursor-pointer space-x-3">
                  <span className="label-text">Remember me</span>
                  <input
                    type="checkbox"
                    className="checkbox checkbox-primary"
                  />
                </label>
              </div>

              <div className="text-sm">
                <a
                  href="/"
                  className="font-medium text-primary hover:text-primary-focus"
                >
                  Forgot your password?
                </a>
              </div>
            </div>

            <div className="flex justify-center">
              <button type="submit" className="btn btn-primary w-full">
                Sign In
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
