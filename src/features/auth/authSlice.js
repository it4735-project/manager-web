import { createSlice } from '@reduxjs/toolkit';
import authService from './authService';

const initialState = {
  currentUser: undefined,
  isAuth: false,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    signout(state) {
      state.isAuth = false;
      state.currentUser = undefined;

      authService.signout();
    },
    setCurrentUser(state, action) {
      state.currentUser = action.payload;
    },
    setIsAuth(state, action) {
      state.isAuth = action.payload;
    },
  },
});

export const authActions = authSlice.actions;

export const selectIsAuth = (state) => state.auth.isAuth;
export const selectCurrentUser = (state) => state.auth.currentUser;

export default authSlice.reducer;
