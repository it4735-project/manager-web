import React from 'react';
import { twMerge } from 'tailwind-merge';
import { useQuery } from '@tanstack/react-query';
import userService from 'src/features/users/userService';
import { MdDelete, MdEdit } from 'react-icons/md';

function UsersPage() {
  const { data: users, status } = useQuery({
    queryKey: ['users'],
    queryFn: () => userService.getAllUsers(),
  });

  return (
    <div className="overflow-x-auto w-full p-3">
      <table className="table w-full">
        <thead>
          <tr>
            <th>
              <label>
                <input type="checkbox" className="checkbox" />
              </label>
            </th>
            <th>Name</th>
            <th>Email</th>
            <th>Address</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {status === 'success' &&
            users?.map((user) => (
              <tr key={user.id}>
                <th>
                  <label>
                    <input type="checkbox" className="checkbox" />
                  </label>
                </th>
                <td>
                  <div className="flex items-center gap-4">
                    <div className="avatar">
                      <div className="mask mask-squircle w-12 h-12">
                        <img
                          src={`https://i.pravatar.cc/150?u=${user.id}`}
                          alt="avatar"
                        />
                      </div>
                    </div>
                    <div className="flex flex-col">
                      <div className="font-bold">{user.name}</div>
                      <div
                        className={twMerge(
                          'badge badge-sm badge-outline',
                          user.role === 'admin' ? 'badge-primary' : ''
                        )}
                      >
                        {user.role}
                      </div>
                    </div>
                  </div>
                </td>
                <td>{user.email}</td>
                <td>{user.address}</td>
                <td>
                  <div className="flex items-center gap-4 justify-end">
                    <button className="btn btn-sm btn-outline btn-warning">
                      <MdEdit className='text-lg'/>
                    </button>
                    <button className="btn btn-sm btn-outline btn-error">
                      <MdDelete className='text-lg'/>
                    </button>
                  </div>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}

export default UsersPage;
