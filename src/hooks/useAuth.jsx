import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@tanstack/react-query';
import authService from 'src/features/auth/authService';
import appService from 'src/features/app/appService';
import { useLocation, useNavigate } from 'react-router';
import { authActions, selectIsAuth } from 'src/features/auth/authSlice';

const useAuth = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();

  const isAuth = useSelector(selectIsAuth);

  const { isLoading, isError, isSuccess } = useQuery({
    queryKey: ['currentUser', isAuth],
    queryFn: () => authService.getMe(),
    retry: false,
    onSuccess: (data) => {
      dispatch(authActions.setCurrentUser(data));
    },
  });

  if (isLoading) {
    appService.showLoadingModal({ fullScreen: true });
  } else if (isError) {
    appService.hideLoadingModal();
    navigate('/auth/signin', {
      state: {
        from: location.pathname,
      },
    });
  } else if (isSuccess) {
    appService.hideLoadingModal();
  }
};

export default useAuth;
