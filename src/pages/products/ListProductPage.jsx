import React from 'react';
import { twMerge } from 'tailwind-merge';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import productService from 'src/features/products/productService';
import appService from 'src/features/app/appService';

function ProductsPage() {
  const {
    data: products,
    isLoading,
    isSuccess,
  } = useQuery({
    queryKey: ['products'],
    queryFn: () => productService.getListProduct(),
  });

  const queryClient = useQueryClient();
  const listUser = queryClient.getQueryData(['users']);

  if (isLoading) {
    appService.showLoadingModal();
  } else if (isSuccess) {
    appService.hideLoadingModal();
  }

  return (
    <div className="overflow-x-auto w-full p-3">
      <table className="table w-full">
        <thead>
          <tr>
            <th>
              <label>
                <input type="checkbox" className="checkbox" />
              </label>
            </th>
            <th>Name</th>
            <th>Sku</th>
            <th>Owner</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {isSuccess &&
            products?.map((product) => (
              <tr key={product.id}>
                <th>
                  <label>
                    <input type="checkbox" className="checkbox" />
                  </label>
                </th>
                <td>
                  <p className="font-bold">{product.name}</p>
                </td>
                <td>{product.sku}</td>
                <td>
                  {listUser?.find((x) => x.id === product.ownerId)?.email}
                </td>
                <td>
                  <div
                    className={twMerge(
                      'badge',
                      product.status === 'active' && 'badge-success'
                    )}
                  >
                    {product.status}
                  </div>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}

export default ProductsPage;
