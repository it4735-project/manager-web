export const APP_NAME = 'Smart Cradle Management';

export const THEME_VALUES = [
  'light',
  'dark',
  'cupcake',
  'bumblebee',
  'emerald',
  'corporate',
  'synthwave',
  'retro',
  'cyberpunk',
  'valentine',
  'halloween',
  'garden',
  'forest',
  'aqua',
  'lofi',
  'pastel',
  'fantasy',
  'wireframe',
  'black',
  'luxury',
  'dracula',
  'cmyk',
  'autumn',
  'business',
  'acid',
  'lemonade',
  'night',
  'coffee',
  'winter',
];

export const API_ENDPOINT = import.meta.env.VITE_API_ENDPOINT;

export const ACCESS_TOKEN_KEY = 'accessToken';
