import React from 'react';
import SignInPage from 'src/pages/auth/signin/SignInPage';
import SignUpPage from 'src/pages/auth/signup/SignUpPage';
import HomePage from 'src/pages/home/HomePage';
import NotFoundPage from 'src/pages/404/NotFoundPage';
import UsersPage from 'src/pages/users/ListUserPage';
import ProductsPage from 'src/pages/products/ListProductPage';
import MainLayout from 'src/layouts/MainLayout';
import AuthLayout from 'src/layouts/AuthLayout';

const routes = [
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: '/', element: <HomePage /> },
      { path: 'users', element: <UsersPage /> },
      { path: 'products', element: <ProductsPage /> },
      { path: '404', element: <NotFoundPage /> },
      { path: '/', element: <HomePage /> },
      { path: '*', element: <NotFoundPage /> },
    ],
  },
  {
    path: '/auth',
    element: <AuthLayout />,
    children: [
      { path: 'signin', element: <SignInPage /> },
      { path: 'signup', element: <SignUpPage /> },
      { path: '*', element: <NotFoundPage /> },
    ],
  },
];

export default routes;
