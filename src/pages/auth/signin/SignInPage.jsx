import React from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import Input from 'src/pages/auth/_components/Input';
import { useDispatch } from 'react-redux';
import { useMutation } from '@tanstack/react-query';
import { toast } from 'react-toastify';
import authService from 'src/features/auth/authService';
import appService from 'src/features/app/appService';
import { authActions } from 'src/features/auth/authSlice';
import { useNavigate } from 'react-router';

const schema = yup
  .object({
    email: yup
      .string()
      .email('Invalid email address')
      .required('Email is required'),
    password: yup.string().required('Password is required'),
  })
  .required();

export default function SignInPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const { isLoading, mutate } = useMutation({
    mutationFn: (data) => authService.signin(data),
    onSuccess: (res) => {
      appService.hideLoadingModal();
      toast.success('Signin successfully');

      dispatch(authActions.setIsAuth(true));
      dispatch(authActions.setCurrentUser(res));

      navigate('/');
    },
    onError: () => {
      appService.hideLoadingModal();
      toast.error('Wrong email or password');
    },
  });

  const onSubmit = (data) => mutate(data);

  if (isLoading) {
    appService.showLoadingModal();
  }

  return (
    <>
      <div className="flex min-h-full items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className="w-full max-w-md space-y-8">
          <div>
            <h1 className="text-center text-5xl font-bold text-primary tracking-tight">
              Smart Cradle
            </h1>
            <h2 className="mt-6 text-center text-2xl tracking-tight">
              Sign in to your account
            </h2>
          </div>

          <form className="mt-8 space-y-3" onSubmit={handleSubmit(onSubmit)}>
            <input type="hidden" name="remember" defaultValue="true" />
            <Input
              name="email"
              label="Email address"
              register={register}
              error={errors.email}
              placeholder="Enter your email address"
            />

            <Input
              name="password"
              label="Password"
              register={register}
              error={errors.password}
              placeholder="Enter your password"
              type="password"
            />

            <div className="flex items-center justify-between">
              <div className="form-control">
                <label className="label cursor-pointer space-x-3">
                  <span className="label-text">Remember me</span>
                  <input
                    type="checkbox"
                    className="checkbox checkbox-primary"
                  />
                </label>
              </div>

              <div className="text-sm">
                <a
                  href="/"
                  className="font-medium text-primary hover:text-primary-focus"
                >
                  Forgot your password?
                </a>
              </div>
            </div>

            <div className="flex justify-center">
              <button type="submit" className="btn btn-primary w-full">
                Sign In
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
}
