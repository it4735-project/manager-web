import httpService from 'src/services/httpService';

class ProductService {
  async getListProduct() {
    const res = await httpService.request({
      url: '/api/admin/cradles',
      method: 'GET',
    });

    return res.data;
  }

  async getProductById(id) {
    const res = await httpService.request({
      url: `/api/admin/cradles/${id}`,
      method: 'GET',
    });

    return res.data;
  }

  async createProduct(data) {
    const res = await httpService.request({
      url: '/api/cradle/',
      method: 'POST',
      data,
    });

    return res;
  }

  async updateProduct(id, data) {
    const res = await httpService.request({
      url: `/api/admin/cradles/${id}`,
      method: 'PUT',
      data,
    });

    return res;
  }

  async deleteProduct(id) {
    const res = await httpService.request({
      url: `/api/admin/cradles/${id}`,
      method: 'DELETE',
    });

    return res;
  }
}

export default new ProductService();
