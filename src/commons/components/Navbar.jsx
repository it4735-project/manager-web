import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { MdKeyboardArrowDown as DownIcon } from 'react-icons/md';
import { THEME_VALUES } from 'src/app/constants';
import { changeTheme } from 'src/features/app/appSlice';
import { Dropdown, DropdownContent, DropdownItem } from './Dropdown';
import { authActions, selectCurrentUser } from 'src/features/auth/authSlice';
import appService from 'src/features/app/appService';

function Navbar() {
  const dispatch = useDispatch();

  const userData = useSelector(selectCurrentUser);

  return (
    <div className="navbar bg-base-100">
      <div className="flex-1">
        <a href="/" className="lg:hidden btn btn-ghost normal-case text-xl">
          Smart Cradle
        </a>
      </div>

      <div className="flex-none">
        <Dropdown
          className="dropdown-end"
          labelComponent={
            <label tabIndex={0} className="btn-ghost btn-sm btn mr-3">
              Theme
              <DownIcon className="text-xl" />
            </label>
          }
        >
          <DropdownContent className="flex h-60 w-52 flex-col flex-nowrap overflow-y-scroll">
            {THEME_VALUES.map((theme, index) => (
              <DropdownItem key={index}>
                <button onClick={() => dispatch(changeTheme(theme))}>
                  {theme.toUpperCase()}
                </button>
              </DropdownItem>
            ))}
          </DropdownContent>
        </Dropdown>

        <Dropdown
          className="dropdown-end"
          labelComponent={
            <label tabIndex={0} className="btn-ghost btn-circle avatar btn">
              <div className="w-10 rounded-full">
                <img
                  src={appService.getMockAvatar(userData?.id)}
                  alt="avatar"
                />
              </div>
            </label>
          }
        >
          <DropdownContent className="w-36">
            <DropdownItem>
              <a href="/profiles">{'Profile'}</a>
            </DropdownItem>

            <DropdownItem>
              <a href="/settings">{'Settings'}</a>
            </DropdownItem>

            <DropdownItem onClick={() => dispatch(authActions.signout())}>
              <button>{'Sign out'}</button>
            </DropdownItem>
          </DropdownContent>
        </Dropdown>
      </div>
    </div>
  );
}

export default Navbar;
