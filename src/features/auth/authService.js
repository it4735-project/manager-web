import { ACCESS_TOKEN_KEY } from 'src/app/constants';
import Cookies from 'js-cookie';
import httpService from 'src/services/httpService';

class AuthService {
  async handleAfterAuth({ accessToken }) {
    Cookies.set(ACCESS_TOKEN_KEY, accessToken);
    const user = await this.getMe();
    return user;
  }

  async signin({ email, password }) {
    const res = await httpService.request({
      url: '/api/auth/login',
      method: 'POST',
      data: {
        email,
        password,
      },
    });

    return await this.handleAfterAuth({ accessToken: res.token });
  }

  async signup({ name, email, password, confirmPassword }) {
    const res = await httpService.request({
      url: '/api/auth/register',
      method: 'POST',
      data: {
        name,
        email,
        password,
        confirmPassword,
      },
    });

    return this.handleAfterAuth({ accessToken: res.token });
  }

  async getMe() {
    const res = await httpService.request({
      url: '/api/user/getme',
      method: 'GET',
    });

    return res.data;
  }

  signout() {
    Cookies.remove(ACCESS_TOKEN_KEY);
  }
}

export default new AuthService();
