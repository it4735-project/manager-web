import React from 'react';
import { twMerge } from 'tailwind-merge';

function Input({ register, name, label, error, ...rest }) {
  return (
    <div className="form-control w-full">
      <label className="label">
        <span className="label-text">{label}</span>
      </label>
      <input
        className={twMerge(
          'input input-bordered w-full',
          error && 'input-error'
        )}
        {...register(name)}
        {...rest}
      />
      {error && (
        <label className="label">
          <span className="label-text-alt text-error">{error.message}</span>
        </label>
      )}
    </div>
  );
}

export default Input;
