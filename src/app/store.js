import { configureStore } from '@reduxjs/toolkit';

import appReducer from 'src/features/app/appSlice';
import authReducer from 'src/features/auth/authSlice';

export const store = configureStore({
  reducer: {
    app: appReducer,
    auth: authReducer,
  },
});
